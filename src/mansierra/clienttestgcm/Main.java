package mansierra.clienttestgcm;

import mansierra.clienttestgcm.gcm.GCMConf;
import mansierra.clienttestgcm.gcm.GoogleGCMserver;
import mansierra.clienttestgcm.gcm.IFCallBack;
import mansierra.clienttestgcm.gcm.Message;
import mansierra.clienttestgcm.gcm.PlayServices;
import mansierra.clienttestgcm.R;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

public class Main extends Activity
{
	private GoogleGCMserver gGCM;
	private String regid;
	private TextView tv_log;
	private Button b_registerGCM;
	private Button b_unRegisterBackEnd;
	private Button b_registerBackEnd;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layo_main);
		InitVars();
		InitGCM();
		InitEvents();
	}

	private void InitVars()
	{
		tv_log = (TextView) findViewById(R.id.layo_main_tv_log);
		b_registerGCM = (Button) findViewById(R.id.layo_main_b_registerGCM);
		b_unRegisterBackEnd = (Button) findViewById(R.id.layo_main_b_unRegisterBackEnd);
		b_registerBackEnd = (Button) findViewById(R.id.layo_main_b_registerBackEnd);
	}

	private void InitGCM()
	{
		if (PlayServices.check(Main.this))
		{
			gGCM = new GoogleGCMserver(Main.this, AppConf.APP_NAME, AppConf.APP_USER);
			regid = gGCM.getRegistrationId(getApplicationContext());
		} else
		{
			Toast.makeText(getApplicationContext(), "PayServices el dispositivo no soportado", Toast.LENGTH_LONG).show();
			b_registerGCM.setEnabled(false);
			b_unRegisterBackEnd.setEnabled(false);
			b_registerBackEnd.setEnabled(false);
			tv_log.setText("PayServices el dispositivo no soportado");
		}
	}

	private void InitEvents()
	{
		b_registerGCM.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				tv_log.setText("Iniciando Registro en GCM y BackEnd\n");
				if (regid.isEmpty())
				{
					gGCM.registerInBackground(
					// Registra en GCM
							new IFCallBack()
							{

								@Override
								public void OnFinish(Bundle datas, Message msg)
								{
									// datas.getString(
									// GCMConf.CALLBACK_SEND_GCM_SERVER_KEY1
									// );
									tv_log.append("GCM Key registrada correctamente\n");
								}

							},
							// Registra en Back-End
							new IFCallBack()
							{

								@Override
								public void OnFinish(Bundle datas, Message msg)
								{
									tv_log.append("GCM Key envidad a Back-End y responde: \n");
									tv_log.append(datas.getString(GCMConf.CALLBACK_SEND_BACKEND_KEY1) + "\n");
								}

							});
				} else
				{
					tv_log.append("El dispositivo ya esta registrado en GCM y su version gPlayServices es correcta.\n");
				}

			}

		});

		b_registerBackEnd.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				tv_log.setText("Iniciando Registro en BackEnd\n");
				gGCM.sendRegistrationIdToBackend(new IFCallBack()
				{

					@Override
					public void OnFinish(Bundle datas, Message msg)
					{
						tv_log.append("GCM Key envidad a Back-End para registro y responde: \n");
						tv_log.append(datas.getString(GCMConf.CALLBACK_SEND_BACKEND_KEY1) + "\n");
					}

				});

			}
		});

		b_unRegisterBackEnd.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				tv_log.setText("Iniciando eliminacion Registro en BackEnd\n");
				tv_log.append("Eliminando almacenamiento local \n");
				gGCM.clearGCMPrefereneces();
				gGCM.sendUnRegistrationIdToBackend(new IFCallBack()
				{

					@Override
					public void OnFinish(Bundle datas, Message msg)
					{
						tv_log.append("GCM Key envidad a Back-End para eliminacion y responde: \n");
						tv_log.append(datas.getString(GCMConf.CALLBACK_SEND_BACKEND_KEY1) + "\n");
					}

				});
			}
		});
	}

	@Override
	public void onResume()
	{
		super.onResume();
		PlayServices.check(this);
	}

}
