package mansierra.clienttestgcm.gcm;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.json.JSONException;

public class Conversions
{
	public static Object FromUTF8InputStream(InputStream is) throws IOException, JSONException
	{
		InputStreamReader reader = new InputStreamReader(is, "UTF-8");
		StringBuilder builder = new StringBuilder();
		char[] buf = new char[1000];
		int l = 0;
		while (l >= 0) {
		    builder.append(buf, 0, l);
		    l = reader.read(buf);
		}

		return builder.toString();
			
	}
}
