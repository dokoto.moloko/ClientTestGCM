package mansierra.clienttestgcm.gcm;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GoogleGCMserver
{

	private static final String API_KEY_REGISTER = "ApiKeyRegister";
	private static final String APP_VERSION = "appVersion";
	private static String TAG = null;
	private static String APP_NAME = null;
	private static String APP_USER = null;

	private GoogleCloudMessaging gcm = null;
	private SharedPreferences prefs = null;
	private Activity activity = null;

	private String ApiKeyRegister = null;
	private int appVersion = 0;

	public GoogleGCMserver(Activity activity, String app_name, String app_user)
	{
		this.activity = activity;
		TAG = this.getClass().getSimpleName();
		APP_NAME = app_name;
		APP_USER = app_user;
	}

	/**
	 * Gets the current registration ID for application on GCM service.
	 * <p>
	 * If result is empty, the app needs to register.
	 * 
	 * @return registration ID, or empty string if there is no existing
	 *         registration ID.
	 */
	public String getRegistrationId(Context context)
	{
		prefs = getGCMPreferences(context);
		ApiKeyRegister = prefs.getString(API_KEY_REGISTER, "");
		if (ApiKeyRegister.isEmpty())
		// if (0 == 0)
		{
			Log.i(TAG, "Registration not found.");
			return new String();
		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		appVersion = prefs.getInt(APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (appVersion != currentVersion)
		{
			Log.i(TAG, "App version changed.");
			return new String();
		}
		return ApiKeyRegister;
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's
	 * shared preferences.
	 */
	public void registerInBackground(final IFCallBack RegisterCallBack, final IFCallBack HttpServerResponse)
	{
		new AsyncTask<Void, Void, Message>()
		{

			@Override
			protected Message doInBackground(Void... params)
			{
				Message msg = new Message();
				try
				{
					if (gcm == null)
					{
						gcm = GoogleCloudMessaging.getInstance(activity.getBaseContext());
					}

					ApiKeyRegister = gcm.register(GCMConf.GOOGLE_PROJECT_ID);
					String messageType = gcm.getMessageType(activity.getIntent());

					if (GoogleCloudMessaging.ERROR_SERVICE_NOT_AVAILABLE.equals(messageType))
					{
						msg.set(Message.Type.ERROR, "Service not Available, please try again later");

					} else if (GoogleCloudMessaging.ERROR_MAIN_THREAD.equals(messageType))
					{
						msg.set(Message.Type.ERROR, "You can register with GCM in Main Thread");

					} else if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType))
					{
						msg.set(Message.Type.ERROR, "Error sending message");

					} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType))
					{
						// It's a regular GCM message, do some work.

						// You should send the registration ID to your server
						// over
						// HTTP, so it can use GCM/HTTP or CCS to send messages
						// to
						// your app. The request to your server should be
						// authenticated
						// if your app is using accounts.
						sendRegistrationIdToBackend(HttpServerResponse);

						// For this demo: we don't need to send it because the
						// device will send upstream messages to a server that
						// echo
						// back the message using the 'from' address in the
						// message.
						// Persist the regID - no need to register again.
						storeRegistrationId(ApiKeyRegister, appVersion);
						msg.set(Message.Type.SUCCESS, "Everything went Ok");
					}
				} catch (IOException ex)
				{
					ApiKeyRegister = activity.getIntent().getStringExtra("registration_id");
					if (ApiKeyRegister != null && !ApiKeyRegister.isEmpty())
					{
						sendRegistrationIdToBackend(HttpServerResponse);
						storeRegistrationId(ApiKeyRegister, appVersion);
						msg.set(Message.Type.WARNING, "Everything went Ok, but I have to use the second way");
					} else
					{
						msg.set(Message.Type.ERROR, "Error :" + ex.getMessage());
						// If there is an error, don't just keep trying to
						// register.
						// Require the user to click a button again, or perform
						// exponential back-off.
					}
				}
				return msg;
			}

			@Override
			protected void onPostExecute(Message msg)
			{
				Bundle datas = new Bundle();
				if (msg.type == Message.Type.SUCCESS)
				{
					datas.putString(GCMConf.CALLBACK_SEND_GCM_SERVER_KEY1, ApiKeyRegister);

				}
				RegisterCallBack.OnFinish(datas, msg);
			}

		}.execute(null, null, null);
	}

	/**
	 * @return Application's {@code SharedPreferences}.
	 */
	private SharedPreferences getGCMPreferences(Context context)
	{
		// This sample app persists the registration ID in shared preferences,
		// but how you store the regID in your app is up to you.
		return activity.getSharedPreferences(activity.getClass().getSimpleName(), Context.MODE_PRIVATE);
	}

	/**
	 * @return Application's version code from the {@code PackageManager}.
	 */
	private int getAppVersion(Context context)
	{
		try
		{
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e)
		{
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Stores the registration ID and app versionCode in the application's
	 * {@code SharedPreferences}.
	 * 
	 * @param context
	 *            application's context.
	 * @param regId
	 *            registration ID
	 */
	private void storeRegistrationId(String ApiKeyRegister, int appVersion)
	{
		prefs = getGCMPreferences(activity.getBaseContext());
		appVersion = getAppVersion(activity.getBaseContext());
		Log.i(TAG, "Saving ApiKeyRegister on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(API_KEY_REGISTER, ApiKeyRegister);
		editor.putInt(APP_VERSION, appVersion);
		editor.commit();
	}

	public void clearGCMPrefereneces()
	{
		SharedPreferences settings = activity.getBaseContext().getSharedPreferences(activity.getClass().getSimpleName(), Context.MODE_PRIVATE);
		settings.edit().clear().commit();
	}

	public void sendRegistrationIdToBackend(final IFCallBack callBack)
	{

		AsyncTask<Void, Void, Message> task = new AsyncTask<Void, Void, Message>()
		{

			HttpClient httpclient = null;
			HttpPost httppost = null;
			HttpResponse response2level = null;
			HttpEntity responseEntity = null;

			@Override
			protected void onPreExecute()
			{
				try
				{
					httpclient = new DefaultHttpClient();
					httppost = new HttpPost(BuildURLwithOUTDatas(GCMConf.REGISTER_METHOD_PATH));
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
					nameValuePairs.add(new BasicNameValuePair(GCMConf.BACKEND_API_KEY_REGISTER, ApiKeyRegister));
					nameValuePairs.add(new BasicNameValuePair(GCMConf.BACKEND_APP_NAME, APP_NAME));
					nameValuePairs.add(new BasicNameValuePair(GCMConf.BACKEND_APP_USER, APP_USER));
					nameValuePairs.add(new BasicNameValuePair(GCMConf.BACKEND_KEY, GetUniqueKey()));
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}

			@Override
			protected Message doInBackground(Void... arg0)
			{
				Message msg = new Message();
				try
				{
					if (httpclient != null)
					{
						response2level = httpclient.execute(httppost);
						responseEntity = response2level.getEntity();
						if (responseEntity != null)
							msg.set(Message.Type.SUCCESS, "EveryThing went OK");
						else
							msg.set(Message.Type.ERROR, "Nothing received");
					} else
					{
						msg.set(Message.Type.ERROR, "Error sending message");
					}
				} catch (Exception e)
				{
					e.printStackTrace();
					msg.set(Message.Type.ERROR, "Exception sending message : " + e.toString());
				}
				return msg;
			}

			@Override
			protected void onPostExecute(Message msg)
			{
				Bundle datas = new Bundle();
				Message newMsg = new Message(msg);
				if (msg.type == Message.Type.SUCCESS)
				{
					try
					{
						datas.putString(GCMConf.CALLBACK_SEND_BACKEND_KEY1, (String) Conversions.FromUTF8InputStream(responseEntity.getContent()));
					} catch (Exception e)
					{
						e.printStackTrace();
						newMsg.set(Message.Type.ERROR, e.toString());
					}

					callBack.OnFinish(datas, newMsg);
				} else
				{
					callBack.OnFinish(datas, msg);
				}

			}
		};
		task.execute((Void[]) null);

	}

	public void sendUnRegistrationIdToBackend(final IFCallBack callBack)
	{

		AsyncTask<Void, Void, Message> task = new AsyncTask<Void, Void, Message>()
		{

			HttpClient httpclient = null;
			HttpPost httppost = null;
			HttpResponse response2level = null;
			HttpEntity responseEntity = null;

			@Override
			protected void onPreExecute()
			{
				try
				{
					httpclient = new DefaultHttpClient();
					httppost = new HttpPost(BuildURLwithOUTDatas(GCMConf.UNREGISTER_METHOD_PATH));
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

					nameValuePairs.add(new BasicNameValuePair(GCMConf.BACKEND_APP_USER, APP_USER));
					nameValuePairs.add(new BasicNameValuePair(GCMConf.BACKEND_APP_NAME, APP_NAME));
					nameValuePairs.add(new BasicNameValuePair(GCMConf.BACKEND_KEY, GetUniqueKey()));

					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}

			@Override
			protected Message doInBackground(Void... arg0)
			{
				Message msg = new Message();
				try
				{
					if (httpclient != null)
					{
						response2level = httpclient.execute(httppost);
						responseEntity = response2level.getEntity();
						if (responseEntity != null)
							msg.set(Message.Type.SUCCESS, "EveryThing went OK");
						else
							msg.set(Message.Type.ERROR, "Nothing received");
					} else
					{
						msg.set(Message.Type.ERROR, "Error sending message");
					}
				} catch (Exception e)
				{
					e.printStackTrace();
					msg.set(Message.Type.ERROR, "Exception sending message : " + e.toString());
				}
				return msg;
			}

			@Override
			protected void onPostExecute(Message msg)
			{
				Message newMsg = new Message(msg);
				Bundle datas = new Bundle();
				if (msg.type == Message.Type.SUCCESS)
				{
					try
					{
						datas.putString(GCMConf.CALLBACK_SEND_BACKEND_KEY1, (String) Conversions.FromUTF8InputStream(responseEntity.getContent()));
					} catch (Exception e)
					{
						e.printStackTrace();
						newMsg.set(Message.Type.ERROR, e.toString());
					}

					callBack.OnFinish(datas, newMsg);
				} else
				{
					callBack.OnFinish(datas, msg);
				}
			}
		};
		task.execute((Void[]) null);

	}

	private String GetUniqueKey()
	{
		TelephonyManager tMgr = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
		return tMgr.getDeviceId();
	}

	private String BuildURLwithOUTDatas(final String method_path) throws UnsupportedEncodingException
	{
		String host = GCMConf.SERVER_PROTOCOL + "://" + GCMConf.SERVER_HOST + ":" + String.valueOf(GCMConf.SERVER_PORT);
		Uri.Builder builderURL = Uri.parse(host).buildUpon();
		builderURL.path(method_path);

		return builderURL.build().toString();

	}

}
