package mansierra.clienttestgcm.gcm;

public class GCMConf
{
	public static final String API_KEY = "AIzaSyBlVIsYPLrkGqXz8rUu5Z6NCMn_csDNde8";
	public static final String GOOGLE_PROJECT_ID = "241101146834";
	public static final String SERVER_PROTOCOL = "http";
	public static final String SERVER_HOST = "mansierra.homelinux.net";
	//public static final String SERVER_HOST = "192.168.77.250";
	public static final int SERVER_PORT = 8080;
	public static final String REGISTER_METHOD_PATH = "TestServGCM/register";
	public static final String UNREGISTER_METHOD_PATH = "TestServGCM/unregister";
	
	public static final String BACKEND_API_KEY_REGISTER = "ApiKeyRegister";
	public static final String BACKEND_KEY = "key";
	public static final String BACKEND_APP_NAME = "AppName";
	public static final String BACKEND_APP_USER = "User";
	
	public static final String CALLBACK_SEND_BACKEND_KEY1 = "HTTPResponse";
	public static final String CALLBACK_SEND_GCM_SERVER_KEY1 = "ApiKeyRegister";
}
