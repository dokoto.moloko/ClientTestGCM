package mansierra.clienttestgcm.gcm;

import mansierra.clienttestgcm.AppConf;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class UpgradePackageReceiver extends BroadcastReceiver
{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		final Context letContext = context;
		GoogleGCMserver gGCM = new GoogleGCMserver((Activity) context, AppConf.APP_NAME, AppConf.APP_USER);
		gGCM.registerInBackground(
		// Registra en GCM
				new IFCallBack()
				{

					@Override
					public void OnFinish(Bundle datas, Message msg)
					{
						if (msg.type == Message.Type.SUCCESS)
						{
							Log.d(msg.type.toString(), msg.description);
							Toast.makeText(letContext, "GCM Register Success : " + msg.description, Toast.LENGTH_SHORT).show();
						} else
						{
							Log.d(msg.type.toString(), msg.description);
							Toast.makeText(letContext, "GCM Register Failed : " + msg.description, Toast.LENGTH_SHORT).show();
						}

					}

				},
				// Registra en Back-End
				new IFCallBack()
				{

					@Override
					public void OnFinish(Bundle datas, Message msg)
					{
						if (msg.type == Message.Type.SUCCESS)
						{
							Log.d(msg.type.toString(), msg.description);
							Toast.makeText(letContext, "Server Back-End Register Success : " + msg.description, Toast.LENGTH_SHORT).show();
						} else
						{
							Log.d(msg.type.toString(), msg.description);
							Toast.makeText(letContext, "Server Back-End Failed : " + msg.description, Toast.LENGTH_SHORT).show();
						}
					}

				});

	}
}
