package mansierra.clienttestgcm.gcm;

import android.app.Activity;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class PlayServices
{
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
	private final static String TAG = "PLayServices";

	public static boolean check(Activity activity)
	{
		int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
		if (resultCode != ConnectionResult.SUCCESS)
		{
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
			{
				GooglePlayServicesUtil.getErrorDialog(resultCode, activity, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else
			{
				Log.i(TAG, "This device is not supported.");
			}
			return false;
		}
		return true;
	}
}
