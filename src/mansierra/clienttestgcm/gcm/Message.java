package mansierra.clienttestgcm.gcm;

public class Message
{
	public enum Type {
		ERROR, INFO, WARNING, SUCCESS
	};

	public Type type;
	public String description;

	public Message()
	{
		this.type = Type.INFO;
		this.description = new String();
	}
	
	public Message(Message msg)
	{
		this.type = msg.type;
		this.description = msg.description;
	}

	public Message(Type type, String description)
	{
		this.type = type;
		this.description = description;
	}

	public Message(Type type)
	{
		this.type = type;
	}

	public Message(String description)
	{
		this.description = description;
	}

	public void set(Type type, String description)
	{
		this.type = type;
		this.description = description;
	}
}
